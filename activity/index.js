let http = require("http");

const server = http.createServer((request, response) => {

    if (request.url == '/login') {
        
        response.writeHead(200, {'Content-type' : 'text/plain'});
        response.end("Welcome to login page");
    }
    else {
        response.writeHead(404, {'Content-Type' : 'text/plain'});
        response.end("Sorry you're looking a page that cannot be found.")
    }
}).listen(3000);

console.log('Server running at http://localhost:3000/');